package com.abdriaeva.selenium.tests;

import com.abdriaeva.pages.TodoListPage;
import com.abdriaeva.selenium.DriverBase;

import org.testng.annotations.Test;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.Assert;


public class TodoIT extends DriverBase {

    private TodoListPage page;

    @BeforeMethod(alwaysRun = true)
    public void initPage() throws Exception {
        page = navigate();
    }

    @AfterMethod(alwaysRun = true)
    public void clearTodoItems() throws Exception {
        if (page.hasTodo()){
            page.filterAll();
            page.removeAllTodoItems();
        }
    }

    private TodoListPage navigate() throws Exception {
        TodoListPage page = new TodoListPage(getDriver());
        page.open();
        return page;
    }

    private String createTodo(TodoListPage page) {
        String title = java.util.UUID.randomUUID().toString();
        page.addTodo(title);
        return title;
    }

    @Test
    public void todoAddedTest() throws Exception {
        String title = createTodo(page);
        Assert.assertTrue(page.hasTodo(title));
    }

    @Test
    public void todoRemovedTest() throws Exception {
        String title = createTodo(page);
        Assert.assertTrue(page.hasTodo(title));
        page.removeTodo(title);
        Assert.assertFalse(page.hasTodo(title));
    }

    @Test
    public void addTodoIncreaseCountTest() throws Exception {
        int count = page.getUncompletedItemsCount();
        createTodo(page);
        Assert.assertEquals(page.getUncompletedItemsCount(), count + 1);
    }

    @Test
    public void todoCompletedTest() throws Exception {
        String title = createTodo(page);
        page.toggleTodo(title);
        Assert.assertTrue(page.isTodoCompleted(title));
    }

    @Test
    public void completeTodoDecreaseCountTest() throws Exception {
        String title = createTodo(page);
        int count = page.getUncompletedItemsCount();
        page.toggleTodo(title);
        Assert.assertEquals(page.getUncompletedItemsCount(), count - 1);
    }

    @Test
    public void reactivateTodoIncreaseCountTest() throws Exception {
        String title = createTodo(page);
        page.toggleTodo(title);
        int count = page.getUncompletedItemsCount();
        page.toggleTodo(title);
        Assert.assertEquals(page.getUncompletedItemsCount(), count + 1);
    }

    @Test
    public void clearCompletedRemovesCompletedTodoItems() throws Exception {
        String title1 = createTodo(page);
        String title2 = createTodo(page);
        page.toggleTodo(title1);
        page.toggleTodo(title2);
        page.clearCompletedTodoItems();
        Assert.assertFalse(page.hasTodo(title1));
        Assert.assertFalse(page.hasTodo(title2));
    }

    @Test
    public void toggleAllCompletesAllTodoItems() throws Exception {
        String title1 = createTodo(page);
        String title2 = createTodo(page);
        page.toggleAllTodoItems();
        Assert.assertTrue(page.isTodoCompleted(title1));
        Assert.assertTrue(page.isTodoCompleted(title2));
    }

    @Test
    public void filterCompletedItemsShowOnlyCompleted() throws Exception {
        String title1 = createTodo(page);
        String title2 = createTodo(page);
        page.toggleTodo(title1);
        page.filterCompleted();
        Assert.assertTrue(page.hasTodo(title1));
        Assert.assertFalse(page.hasTodo(title2));
    }

    @Test
    public void filterActiveItemsShowOnlyUncompleted() throws Exception {
        String title1 = createTodo(page);
        String title2 = createTodo(page);
        page.toggleTodo(title1);
        page.filterActive();
        Assert.assertTrue(page.hasTodo(title2));
        Assert.assertFalse(page.hasTodo(title1));
    }

}