package com.abdriaeva.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;


public class TodoListPage extends BasePage {
    @FindBy(css = "#new-todo")
    protected WebElement todoInput;

    @FindBy(css = "#todo-list")
    protected WebElement todoList;

    @FindBy(css = "#toggle-all")
    protected WebElement toggleAllButton;

    protected By todoItemToggleSelector = By.cssSelector(".toggle");
    protected By todoItemDestroySelector = By.cssSelector(".destroy");

    private By allFilterLink = By.cssSelector("#filters li:nth-child(1) a");
    private By activeFilterLink = By.cssSelector("#filters li:nth-child(2) a");
    private By completedFilterLink = By.cssSelector("#filters li:nth-child(3) a");

    public TodoListPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        getDriver().get("http://todomvc-socketstream.herokuapp.com");
        waitUrl("#/all");
    }

    public void filterAll() {
        safeExecute(() -> getDriver().findElement(allFilterLink).click());
        waitUrl("#/all");
    }

    public void filterActive() {
        safeExecute(() -> getDriver().findElement(activeFilterLink).click());
        waitUrl("#/active");
    }

    public void filterCompleted() {
        safeExecute(() -> getDriver().findElement(completedFilterLink).click());
        waitUrl("#/completed");
    }

    public void addTodo(String title) {
        todoInput.sendKeys(title + Keys.RETURN);
        todoInput.sendKeys(Keys.RETURN);
        safeExecute(() -> waitElementVisible(getTodoElmentSelector(title)));
    }

    public Boolean hasTodo(String title) {
        return todoList.findElements(getTodoElmentSelector(title)).size() > 0;
    }

    public Boolean hasTodo() {
        return getListItemsCount() > 0;
    }

    public void toggleAllTodoItems() {
        toggleAllButton.click();
    }

    public void clearCompletedTodoItems() {
        safeExecute(() -> clearCompletedButton().click());
    }

    public Boolean isTodoCompleted(String title) {
        return safeGet(() -> getTodoListItem(title).getAttribute("class").contains("completed"));
    }

    public void removeTodo(String title) {
        safeExecute(() -> {
            Actions action = new Actions(getDriver());
            action
                .moveToElement(getTodoListItem(title))
                .moveToElement(getTodoListItemDestroy(title))
                .click()
                .build()
                .perform();
        });
        waitElemenNotVisible(getTodoElmentSelector(title));
    }

    public void toggleTodo(String title) {
        safeExecute(() -> {
            waitElementClickable(getTodoListItemToggle(title)).click();
        });
    }

    public int getUncompletedItemsCount() {
        return Integer.parseInt(safeGet(() -> itemCountLabel().getText()));
    }

    public int getListItemsCount() {
        return todoList.findElements(By.cssSelector("li")).size();
    }

    public void removeAllTodoItems() {
        for (int i = 0; i < todoList.findElements(By.cssSelector("li label")).size(); i++) {
            String title = safeGet(() -> todoList.findElements(By.cssSelector("li label")).get(0).getText());
            removeTodo(title);
        }
    }

    protected WebElement itemCountLabel() {
        return getDriver().findElement(By.cssSelector("#todo-count strong"));
    }

    protected WebElement clearCompletedButton() {
        return scrollToElement(waitElementVisible(By.cssSelector("#clear-completed")));
    }

    protected WebElement getTodoListItem(String title) {
        return scrollToElement(waitElementVisible(getTodoElmentSelector(title)));
    }

    protected WebElement getTodoListItemToggle(String title) {
        return getTodoListItem(title).findElement(todoItemToggleSelector);
    }

    protected WebElement getTodoListItemDestroy(String title) {
        return getTodoListItem(title).findElement(todoItemDestroySelector);
    }

    protected By getTodoElmentSelector(String title) {
        return By.xpath("//li[.//label[contains(text(),'"+ title +"')]]");
    }
}