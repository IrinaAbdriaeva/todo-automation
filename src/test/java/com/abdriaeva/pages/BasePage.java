package com.abdriaeva.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.interactions.Actions;

import java.util.function.Supplier;
import java.lang.Runnable;

public abstract class BasePage {
    private WebDriver webDriver;

    public BasePage(WebDriver driver) {
        webDriver = driver;
        PageFactory.initElements(driver, this);
    }

    protected WebDriver getDriver() {
        return webDriver;
    }

    private WebDriverWait createDefaultWait() {
        return new WebDriverWait(webDriver, WebDriverWait.DEFAULT_SLEEP_TIMEOUT);
    }

    protected void waitElemenNotVisible(By by) {
        new WebDriverWait(getDriver(), 3).until(ExpectedConditions.invisibilityOfElementLocated(by));
    }

    protected WebElement waitElementVisible(WebElement element) {
        return createDefaultWait().until(ExpectedConditions.visibilityOf(element));
    }

    protected WebElement waitElementVisible(By by) {
        return waitElementVisible(
            createDefaultWait().until(ExpectedConditions.presenceOfElementLocated(by))
        );
    }

    protected WebElement waitElementClickable(WebElement element) {
        return createDefaultWait().until(ExpectedConditions.elementToBeClickable(element));
    }

    protected void waitUrl(String url) {
        createDefaultWait().until(ExpectedConditions.urlContains(url));
    }

    protected WebElement scrollToElement(WebElement element) {
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
        waitFor(500);
        return element;
    }

    protected void waitFor(long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
        }
    }

    protected void safeExecute(Runnable f) {
        safeExecute(f, 0);
    }

    private void safeExecute(Runnable f, int times) {
        try {
            f.run();
        } catch (StaleElementReferenceException ex) {
            if (times < 10) {
                safeExecute(f, times++);
            } else {
                f.run();
            }
        }
    }

    protected <T> T safeGet(Supplier<T> f) {
        return safeGet(f, 0);
    }

    private <T> T safeGet(Supplier<T> f, int times) {
        try {
            return f.get();
        } catch (StaleElementReferenceException ex) {
            if (times < 10) {
                return safeGet(f);
            } else {
                return f.get();
            }
        }
    }
}