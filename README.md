Selenium Tests for TODO
=======================

This project was created using [Selenium-Maven-Template](https://github.com/Ardesco/Selenium-Maven-Template).

To run the tests in different modes refer to the link above. The simple steps are the following:

1. Clone this project
2. `cd selenium-todo` or you custom name if you used one
3. `mvn clean verify`

# Test results

Once the tests are run the folder `failsafe-reports` is created in the `target` folder. Navigate to the `./target/failsafe-reports` and open `index.html` file in any preferred browser to see test run report and any errors. 

The `testng-results.xml` file contains test results in XML format.

If there are any failed tests there will be screenshots created and placed to `./target/screenshots` folder with the name of the test.